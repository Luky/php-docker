#!/usr/bin/env bash

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

mkdir -p out

_b() {
  docker build $1 --tag arziel/php:$1 >out/$1.log
}


set -e

docker build 8.1 --tag arziel/php:8.1
