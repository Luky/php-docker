


# -----------------------------------------------
# igbinary
# -----------------------------------------------

RUN pecl install igbinary && docker-php-ext-enable igbinary


# -----------------------------------------------
# Soap
# -----------------------------------------------


RUN set -e && \
    	apt-get update && apt-get install --no-install-recommends -y \
            libxml2-dev \
    	&& rm -rf /var/lib/apt/lists/*

RUN docker-php-ext-install soap


# -----------------------------------------------
# etc.
# -----------------------------------------------

LABEL maintener="arziel12@gmail.com"


WORKDIR /var/docker

RUN rm -rf /var/lib/apt/lists/*
