#!/usr/bin/env bash

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

header() {
		echo "# -------------------------------------------------------------"
		echo "# @Generated $(date)"
		echo "#"
		echo "# Don't edit this file directly!"
		echo "# For edit use Docker file in root directory & run ./copy.sh"
		echo "# to regenerate"
		echo "# -------------------------------------------------------------"
		echo; echo
}

generate() {
	mkdir -p $1
	FROM=$2

	if [[ $FROM == "" ]]; then
		FROM=$1
	fi

	echo "Copy $1"
	{
		## Header
		header

		## FROM
		echo FROM php:$FROM

		cat ${DIR}/includes/deps.Dockerfile
		cat ${DIR}/includes/core.Dockerfile
		cat ${DIR}/includes/composer.Dockerfile
		cat ${DIR}/includes/yarn.Dockerfile

		cat ${DIR}/includes/redis.Dockerfile
		cat ${DIR}/includes/xdebug.Dockerfile

		## GD
		cat ${DIR}/includes/gd.Dockerfile

		## Footer
		cat ${DIR}/includes/footer.Dockerfile

	} > ${DIR}/$1/Dockerfile
}


generate 8.0 8.0-fpm
generate 8.1 8.1-fpm
