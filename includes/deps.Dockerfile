
# -----------------------------------------------
# Dependencies
# -----------------------------------------------

RUN set -e && \
	apt-get update && apt-get install --no-install-recommends -y \
		wget \
		rsyslog \
        libfreetype6-dev \
        libjpeg62-turbo-dev \
        libmcrypt-dev \
        ssh \
        git \
        telnet \
        unzip \
        openssl \
        libzip-dev \
        libxml2 \
        zlib1g-dev \
	&& rm -rf /var/lib/apt/lists/*
